class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Message {
  // register
  static const REGITSER_SUCECSS = "Register Berhasil";
  static const REGISTER_FAILED = "Register Gagal";

  // login
  static const LOGIN_SUCCESS = "Login Berhasil";
  static const LOGIN_FAILED = "Login Gagal, Periksa kembali inputan anda";

  // error
  static const UNKNOWN_ERROR = 'Terjadi Kesalahan';
  static const NO_INTERNET_ERROR =
      'Tidak dapat terhubung ke server, pastikan perangkat anda terhubung ke internet.';
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
