import 'dart:io';
import 'package:dio/dio.dart';
import 'package:majootestcase/utils/constant.dart';

class ErrorHelper {
  static String getErrorMessage(dynamic error) {
    String message = "Something went wrong";

    if (error is DioError) {
      if (error.error is SocketException || error.type == DioErrorType.connectTimeout) {
        message = Message.NO_INTERNET_ERROR;
      } else if (error.response != null && error.response?.data['message'] != null) {
        message = error.response?.data['message'];
      } else {
        message = error.message;
      }
    }
    return message;
  }
}
