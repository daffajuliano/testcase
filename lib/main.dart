import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/ui/login/register_page.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter/material.dart';

import 'ui/extra/loading.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthBlocCubit()..fetchStatusLogin(),
      child: MaterialApp(
        title: 'Testcase',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBlocCubit, AuthBlocState>(
      listener: (context, state) {
        if (state.message != null) {
          Fluttertoast.showToast(msg: state.message!);
        }
      },
      builder: (context, state) {
        if (state is AuthBlocLoggedInState) {
          return HomePage();
        } else if (state is AuthBlocLoadingState) {
          return LoadingIndicator();
        } else {
          if (state is AuthBlocLoginState) {
            // check if selected page is login or register
            if (state.isRegister) {
              return RegisterPage();
            } else {
              return LoginPage();
            }
          } else if (state is AuthBlocErrorState) {
            // check if selected page is login or register
            if (state.isRegister) {
              return RegisterPage();
            } else {
              return LoginPage();
            }
          } else {
            // login is default page
            return LoginPage();
          }
        }
      },
    );
  }
}
