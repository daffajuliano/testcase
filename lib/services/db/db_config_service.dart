import 'package:sqflite/sqflite.dart' as sqlite;
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart' as path;

class DbConfigService {
  // make singleton
  static DbConfigService _dbHelper = DbConfigService._singleton();

  factory DbConfigService() {
    return _dbHelper;
  }

  DbConfigService._singleton();

  // make tables
  static final String userTable =
      " CREATE TABLE IF NOT EXISTS users ( id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT , email TEXT, password TEXT) ";

  final tables = [
    userTable,
  ];

  Future<Database> openDB() async {
    final dbPath = await sqlite.getDatabasesPath();
    return sqlite.openDatabase(path.join(dbPath, 'testcase.db'), onCreate: (db, version) {
      tables.forEach((table) async {
        await db.execute(table).then((value) {
          print("Berhasil membuat database");
        }).catchError((err) {
          print("error: ${err.toString()}");
        });
      });
      print('Table Created');
    }, version: 1);
  }

  insert(String table, Map<String, Object> data) {
    openDB().then((db) {
      db.insert(table, data, conflictAlgorithm: ConflictAlgorithm.replace);
    }).catchError((err) {
      print("error: $err");
    });
  }

  Future<List> getData(String tableName) async {
    final db = await openDB();
    var result = await db.query(tableName);
    return result.toList();
  }
}
