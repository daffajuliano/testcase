import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/db/db_config_service.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:sqflite/sqflite.dart';

class DbService {
  Future<String> addUser(User data) async {
    try {
      final Database db = await DbConfigService().openDB();

      // get user data
      int result = await db.insert('users', data.toJson());

      // check result lenght
      if (result > 0) {
        return Message.REGITSER_SUCECSS;
      } else {
        return Message.REGISTER_FAILED;
      }
    } catch (e) {
      print(e);
      // return false when error
      return Message.UNKNOWN_ERROR;
    }
  }

  Future<bool> getLogin(User data) async {
    try {
      final Database db = await DbConfigService().openDB();

      // get user data
      List<Map<String, dynamic>> result = await db.query(
        'users',
        where: 'email = ? and password = ?',
        whereArgs: [data.email, data.password],
        limit: 1,
      );

      // check result lenght
      if (result.length > 0) {
        print('a');
        return true;
      } else {
        print('b');
        return false;
      }
    } catch (e) {
      print(e);
      // return fale when error
      return false;
    }
  }
}
