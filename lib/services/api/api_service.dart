import 'package:dio/dio.dart';
import 'package:fpdart/fpdart.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api/dio_config_service.dart' as dioConfig;
import 'package:majootestcase/utils/error_helper.dart';

class ApiServices {
  Future<Either<String, MovieResponse>> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response response = await dio.get("auto-complete?q=game%20of%20thr");
      MovieResponse movieResponse = MovieResponse.fromJson(response.data);
      return Right(movieResponse);
    } catch (e) {
      return Left(ErrorHelper.getErrorMessage(e));
    }
  }
}
