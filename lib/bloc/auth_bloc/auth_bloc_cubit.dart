import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/db/db_service.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  // contain current page
  bool isRegister = false;

  void fetchStatusLogin() async {
    emit(AuthBlocLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState(isRegister: isRegister));
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState(isRegister: isRegister));
      }
    }
  }

  void changePage(bool isRegister) {
    emit(AuthBlocLoadingState());

    // save current page
    this.isRegister = isRegister;

    emit(AuthBlocLoginState(isRegister: isRegister));
  }

  void loginUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    // check login from database
    DbService dbService = DbService();
    bool isLogin = await dbService.getLogin(user);
    // delay 100ms
    Future.delayed(Duration(milliseconds: 100));

    if (isLogin) {
      await sharedPreferences.setBool("is_logged_in", true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);
      emit(AuthBlocLoggedInState(message: Message.LOGIN_SUCCESS));
    } else {
      print('error');
      emit(AuthBlocErrorState(message: Message.LOGIN_FAILED, isRegister: isRegister));
    }
  }

  void registerUser(User user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    // check login from database
    DbService dbService = DbService();
    String result = await dbService.addUser(user);
    if (result != Message.UNKNOWN_ERROR) {
      await sharedPreferences.setBool("is_logged_in", true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);
      emit(AuthBlocLoggedInState(message: Message.REGITSER_SUCECSS));
    } else {
      emit(AuthBlocErrorState(message: Message.REGISTER_FAILED, isRegister: isRegister));
    }
  }

  void logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    // set is_logged_in to false
    await sharedPreferences.setBool("is_logged_in", false);

    // set default page to login when logged out
    this.isRegister = false;

    emit(AuthBlocLoginState(isRegister: isRegister));
  }
}
