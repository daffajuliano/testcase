part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  final String? message;

  const AuthBlocState(this.message);

  @override
  List<Object?> get props => [message];
}

class AuthBlocInitialState extends AuthBlocState {
  AuthBlocInitialState() : super(null);
}

class AuthBlocLoadingState extends AuthBlocState {
  AuthBlocLoadingState() : super(null);
}

class AuthBlocLoggedInState extends AuthBlocState {
  final String? message;

  AuthBlocLoggedInState({this.message}) : super(message);

  @override
  List<Object?> get props => [message];
}

class AuthBlocLoginState extends AuthBlocState {
  final String? message;
  final bool isRegister;

  AuthBlocLoginState({
    this.message,
    required this.isRegister,
  }) : super(message);

  @override
  List<Object?> get props => [message, isRegister];
}

class AuthBlocErrorState extends AuthBlocState {
  final String? message;
  final bool isRegister;

  AuthBlocErrorState({
    this.message,
    required this.isRegister,
  }) : super(message);

  @override
  List<Object?> get props => [message, isRegister];
}

class AuthBlocSuccesState extends AuthBlocState {
  AuthBlocSuccesState() : super(null);
}

class AuthBlocLoadedState extends AuthBlocState {
  final data;

  AuthBlocLoadedState(this.data) : super(null);

  @override
  List<Object> get props => [data];
}
