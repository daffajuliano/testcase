import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fpdart/fpdart.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/services/api/api_service.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocLoadingState());

    ApiServices apiServices = ApiServices();
    // get movie from service
    Either<String, MovieResponse> response = await apiServices.getMovieList();

    // check if response is success or failure
    response.fold(
      (failure) => emit(HomeBlocErrorState(failure)),
      (result) => emit(HomeBlocLoadedState(result.data)),
    );
  }
}
