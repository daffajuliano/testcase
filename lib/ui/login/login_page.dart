import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController(initialValue: '');
  final _passwordController = TextController(initialValue: '');
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Selamat Datang',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  // color: colorBlue,
                ),
              ),
              Text(
                'Silahkan login terlebih dahulu',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 9,
              ),
              _form(),
              SizedBox(
                height: 50,
              ),
              CustomButton(
                text: 'Login',
                onPressed: handleLogin,
                height: 100,
              ),
              SizedBox(
                height: 50,
              ),
              _register(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) return pattern.hasMatch(val) ? null : 'Masukkan e-mail yang valid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'Password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          SchedulerBinding.instance!.addPostFrameCallback((timeStamp) {
            BlocProvider.of<AuthBlocCubit>(context).changePage(true);
          });
        },
        child: RichText(
          text: TextSpan(text: 'Belum punya akun? ', style: TextStyle(color: Colors.black45), children: [
            TextSpan(
              text: 'Daftar',
            ),
          ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true) {
      User user = User(
        email: _email,
        password: _password,
      );

      // login will be excecuted after all frame rendered,
      // because bloc listener will be bugging if not use addPostFrameCallback
      SchedulerBinding.instance!.addPostFrameCallback((timeStamp) {
        BlocProvider.of<AuthBlocCubit>(context).loginUser(user);
      });
    } else {
      // check for any empty field
      if (_emailController.value.isEmpty || _passwordController.value.isEmpty) {
        Fluttertoast.showToast(msg: 'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan');
      }
    }
  }
}
