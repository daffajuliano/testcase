import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/extra/shimmer_image.dart';

class ListItemSeries extends StatelessWidget {
  final Series series;

  const ListItemSeries({Key? key, required this.series}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double imageWidth = 70;
    final double imageHeight = 70;

    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              child: CachedNetworkImage(
                imageUrl: series.i.imageUrl,
                placeholder: (context, url) {
                  return ShimmerImage(width: imageWidth, height: imageHeight);
                },
                errorWidget: (context, url, error) => Container(
                  width: imageWidth,
                  height: imageHeight,
                  color: Colors.grey,
                  child: Center(
                    child: Icon(Icons.report_problem_outlined, color: Colors.black),
                  ),
                ),
                width: imageWidth,
                height: imageHeight,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(width: 10.0),
            Expanded(
              child: Text(
                series.l,
                maxLines: 2,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
