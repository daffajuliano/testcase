import 'package:flutter/material.dart';
import 'package:majootestcase/utils/constant.dart';

class ErrorPage extends StatelessWidget {
  final String message;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorPage(
      {Key? key,
      this.gap = 10,
      this.retryButton,
      this.message = "",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                message == Message.NO_INTERNET_ERROR
                    ? Icons.wifi_off_outlined
                    : Icons.report_problem_outlined,
                color: textColor ?? Colors.black,
                size: 50,
              ),
              const SizedBox(height: 20),
              Text(
                message,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  color: textColor ?? Colors.black,
                ),
              ),
              retry != null
                  ? Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),
                        retryButton ??
                            ElevatedButton.icon(
                              onPressed: () {
                                if (retry != null) retry!();
                              },
                              icon: Icon(Icons.refresh_sharp, color: Colors.black),
                              label: Text(
                                'Retry',
                                style: TextStyle(color: Colors.black),
                              ),
                              style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                              ),
                            ),
                      ],
                    )
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
