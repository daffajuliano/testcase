import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_response.dart';
import 'package:majootestcase/ui/extra/list_item_movie.dart';
import 'package:majootestcase/ui/extra/shimmer_image.dart';
import 'package:majootestcase/ui/home/movie_detail_page.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../extra/loading.dart';
import '../extra/error_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBlocCubit()..fetchingData(),
      child: _content(context),
    );
  }

  Widget _content(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Home',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () => BlocProvider.of<AuthBlocCubit>(context).logout(),
            icon: Icon(
              Icons.logout,
              color: Colors.black,
            ),
          ),
        ],
      ),
      body: BlocBuilder<HomeBlocCubit, HomeBlocState>(
        builder: (context, state) {
          if (state is HomeBlocLoadedState) {
            return _loaded(context, state.data);
          } else if (state is HomeBlocLoadingState) {
            return _loadingShimmer(context);
          } else if (state is HomeBlocInitialState) {
            return LoadingIndicator();
          } else if (state is HomeBlocErrorState) {
            return ErrorPage(
              message: state.error,
              retry: () => BlocProvider.of<HomeBlocCubit>(context).fetchingData(),
            );
          }

          return Center(child: Text(kDebugMode ? "state not implemented $state" : ""));
        },
      ),
    );
  }

  Widget _loaded(BuildContext context, List<Data> data) {
    return ListView.builder(
      padding: const EdgeInsets.all(10),
      itemCount: data.length,
      itemBuilder: (context, index) {
        return ListItemMovie(
          data: data[index],
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MovieDetailPage(data: data[index]),
              ),
            );
          },
        );
      },
    );
  }

  _loadingShimmer(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(10),
      itemBuilder: (context, index) => ShimmerImage(
        width: double.infinity,
        height: 200,
      ),
      separatorBuilder: (context, index) => const SizedBox(height: 10),
      itemCount: 10,
    );
  }
}
